# tp5

Java program that implements a simple version of the Google page rank algorithm with the use of apache spark.

### Prerequisites

* You need to have JDK 8 or hire installed.
* You need to have maven installed.
* You need to have spark installed.


### Build

In project's root directory, execute:

``
mvn clean package
``

### Run
To run with apache spark, you need to specify path to spark bin directory in your PATH.
To verify spark is properly installed, run :

``
spark-submit --help
``

It should display help information.

One `spark` works and project is compiled, run:

``
spark-submit --class "com.ulaval.tp5.Program" --master local[*] ./target/tp5.jar -f ./python.org.json
``

You can replace the `-f` argument value with the absolute path to the json file you want the program to use.

The resulting page ranks will be displayed on the console, order by ranks ind descending order, in between the

`... --- RESULT --- ...` and `... --- RESULT END --- ...` lines.


