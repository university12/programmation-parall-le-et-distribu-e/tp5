package com.ulaval.tp5;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ulaval.tp5.exceptions.InvalidFileException;
import com.ulaval.tp5.models.Page;
import com.ulaval.tp5.services.ParallelPageRankService;
import com.ulaval.tp5.services.LinearPageRankService;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import org.apache.commons.cli.ParseException;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import java.time.Instant;
import java.time.Duration;

 
public class Program {
    private static final int ITERATION_COUNT = 100;
    public static void main(String[] args) throws ParseException {
        if (args.length < 2) {
            throw new InvalidFileException("No file provided. length is " + args.length);
        }
        String filePath = "";
        String numberOfInstances = "1";
        for (int i = 0; i < args.length; i ++) {
            if (args[i].equals("-f")) {
                filePath = args[i + 1];
            }
            if (args[i].equals("-n")) {
                numberOfInstances = args[i + 1];
            }
        }
        System.out.println(System.lineSeparator() + "File : " +filePath + System.lineSeparator());
        System.out.println(System.lineSeparator() + "Number of cores : " +numberOfInstances + System.lineSeparator());

        SparkConf config = new SparkConf();
        config.setAppName("Tp5PageRank");
        config.set("spark.executor.cores", numberOfInstances);
        config.set("spark.executor.instances", numberOfInstances);
        config.set("spark.default.parallelism", numberOfInstances);
        
        Instant startLin = Instant.now();
        Instant finishLin = Instant.now();
        Instant start = Instant.now();
        Instant finish = Instant.now();

        try {
        startLin = Instant.now();
        List<Page> pageNodes = readAllPages(filePath);
        LinearPageRankService linearPageRankService = new LinearPageRankService();
        linearPageRankService.run(pageNodes, ITERATION_COUNT);
        finishLin = Instant.now();

        } catch(Exception ex) {
            System.err.println("ERROR : " + System.lineSeparator() + ex.getMessage());
            System.exit(1);
        } 


        JavaSparkContext sparkContext = new JavaSparkContext(config);

        try {
            start = Instant.now();
            List<Page> pageNodes = readAllPages(filePath);
            ParallelPageRankService parallelPageRankService = new ParallelPageRankService();
            parallelPageRankService.run(pageNodes, ITERATION_COUNT, sparkContext);

            finish = Instant.now();
            long timeElapsed = Duration.between(start, finish).toMillis();  //in millis
            System.out.println(System.lineSeparator() + "Execution time : " +timeElapsed + "ms" + System.lineSeparator());


        } catch(Exception ex) {
            System.err.println("ERROR : " + System.lineSeparator() + ex.getMessage());
            System.exit(1);
        } finally{
            sparkContext.stop();
        }
        long timeElapsedPar = Duration.between(start, finish).toMillis();  //in millis
        System.out.println(System.lineSeparator() + " Parallele execution time : " +timeElapsedPar + "ms" + System.lineSeparator());

        long timeElapsed = Duration.between(startLin, finishLin).toMillis();  //in millis
        System.out.println(System.lineSeparator() + " Lineaire execution time : " +timeElapsed + "ms" + System.lineSeparator());
        double speedUp = (double) timeElapsed / (double) timeElapsedPar;
        System.out.println(System.lineSeparator() + " Speedup : " +speedUp + System.lineSeparator());


        System.exit(0);
    }

    private static List<Page> readAllPages(String filePath) throws IOException {
        if (!new File(filePath).exists()) {
            throw new InvalidFileException("FILE does not exists");
        }

        StringBuilder contentBuilder = new StringBuilder();
        Files.lines(Paths.get(filePath), StandardCharsets.UTF_8).forEach(s -> contentBuilder.append(s).append("\n"));
        String content = contentBuilder.toString();

        ObjectMapper mapper = new ObjectMapper();

        return mapper.readValue(content, new TypeReference<List<Page>>(){});
    }
}
