package com.ulaval.tp5.exceptions;

public class InvalidFileException extends RuntimeException {

  public InvalidFileException(String message) {
    super(InvalidFileException.class.getName() + System.lineSeparator() + message);
  }
}
