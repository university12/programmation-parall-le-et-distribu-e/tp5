package com.ulaval.tp5.models;

import java.io.Serializable;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Page implements Serializable {

  @EqualsAndHashCode.Include
  private Integer id;
  private String url;
  private Long size;
  private List<Integer> neighbors;
}
