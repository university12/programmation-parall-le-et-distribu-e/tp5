package com.ulaval.tp5.services;

import com.google.common.collect.Iterables;
import com.ulaval.tp5.models.Page;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function2;
import scala.Tuple2;

public class ParallelPageRankService {
  public void run(List<Page> pages, int iterationCount, JavaSparkContext sparkContext) {


    JavaRDD<Page> pagesRDD = sparkContext.parallelize(pages);

    JavaPairRDD<Page, Iterable<Page>> pagesGraph = pagesRDD.mapToPair(page -> {
      List<Integer> neighborIds = page.getNeighbors();
      List<Page> neighborsList = new ArrayList<>();
      neighborIds.forEach(id -> pages.stream()
          .filter(p -> p.getId().equals(id))
          .findFirst()
          .ifPresent(neighborsList::add));

      return new Tuple2<>(page, (Iterable<Page>) neighborsList);
    }).distinct().cache();

    JavaPairRDD<Page, Double> rankGraph = pagesGraph.mapValues(page -> 1.0).cache();


    for (int current = 0; current < iterationCount; current++) {

      JavaPairRDD<Page, Double> contributions = pagesGraph
          .join(rankGraph)
          .values()
          .flatMapToPair(neighborsAndRank -> {
            int neighborCount = Iterables.size(neighborsAndRank._1());
            List<Tuple2<Page, Double>> results = new ArrayList<>();
            for (Page neighbor : neighborsAndRank._1) {
              Double value = neighborCount!=0 ? neighborsAndRank._2() / neighborCount:0;
              results.add(new Tuple2<>(neighbor, value));
            }
            return results.iterator();
          });

      rankGraph = contributions.reduceByKey(new Sum()).mapValues(sum -> (1 - 0.85) + sum * 0.85);
    }

    List<Tuple2<Double, Page>> output = rankGraph.mapToPair(Tuple2::swap).sortByKey(false).collect();
    System.out.println(System.lineSeparator() + "-------------------- RESULTS --------------------------------" + System.lineSeparator());
    for (Tuple2<Double, Page> tuple : output) {
      System.out.println(tuple._2().getUrl() + " has rank: " + tuple._1() + ".");
    }
    System.out.println(System.lineSeparator() + "-------------------- RESULTS END--------------------------------" + System.lineSeparator());
  }

  private static class Sum implements Function2<Double, Double, Double> {
    @Override
    public Double call(Double a, Double b) {
      return a + b;
    }
  }
}

// 755837 1 core
// 867090 2 core
// 843675ms 10 core
// 246938ms
