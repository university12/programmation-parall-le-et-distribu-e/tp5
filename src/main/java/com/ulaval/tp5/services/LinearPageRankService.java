package com.ulaval.tp5.services;

import com.ulaval.tp5.models.Page;
import java.util.ArrayList;
import java.util.List; 
import java.util.Map;
import java.util.HashMap;
import scala.Tuple2;
import java.util.Collections;
import java.util.Comparator;

public class LinearPageRankService {
    public void run(List<Page> pages, int iterationCount) {
        Map<Page,Double>rank = new HashMap<Page, Double>();
        for(Page page: pages){
            rank.put(page,1.0);
        }

        for (int i = 0;i<iterationCount;i++){
            System.out.println(System.lineSeparator() + "-------------------- ITERATION: " + i + "--------------------------------" + System.lineSeparator());
             for(Page page: pages){
                List<Integer> neighborIds = page.getNeighbors();
                List<Page> neighborsList = new ArrayList<>();
                neighborIds.forEach(id -> pages.stream()
                    .filter(p -> p.getId().equals(id))
                    .findFirst()
                    .ifPresent(neighborsList::add));
                double newRank = 0.0;
                for(Page neighbor: neighborsList){
                    if (neighbor.getNeighbors().size()!=0){
                        newRank+= rank.get(neighbor)/ (double) neighbor.getNeighbors().size();
                    }

                }
                newRank*=0.85;
                newRank+=(1 - 0.85);
                rank.put(page,newRank);
            }
        }

    List< Tuple2<String,Double>> output = new ArrayList<>();
    for (Map.Entry<Page,Double> result : rank.entrySet()) {
        Tuple2<String,Double> currentVal =  new Tuple2<String,Double>(result.getKey().getUrl(),result.getValue());
        output.add(currentVal);
    }
    output.sort(new PageComparator());


    System.out.println(System.lineSeparator() + "-------------------- RESULTS --------------------------------" + System.lineSeparator());
    for (Tuple2<String,Double> result : output) {
      System.out.println(result._1+ " has rank: " + result._2 + ".");
    }
    System.out.println(System.lineSeparator() + "-------------------- RESULTS END--------------------------------" + System.lineSeparator());
  }

  private static class PageComparator implements Comparator<Tuple2<String,Double>>{
    public int compare(Tuple2<String,Double> a, Tuple2<String,Double> b){
      return b._2.compareTo(a._2);
    }
  }
}
