build:
	mvn clean package
run:
	spark-submit --class "com.ulaval.tp5.Program" --master local[*] ./target/tp5.jar -f $(f) -n $(n)
run-test:
		spark-submit --class "com.ulaval.tp5.Program" --master local[*] ./target/tp5.jar -f ./python.org.json -n 10
